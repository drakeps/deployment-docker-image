#!/bin/bash

if [ "$HOST_USERNAME" = "root" ]; then
  echo -e "\e[91mПользователь не может быть root \e[0m\n";
  exit 1;
fi

# Если ключи получили через volumes 
if [ -d "/root/.ssh" ]; then
  echo -e "\n\e[92mSSH ключи добавлены\e[0m";

  echo -e "\n\e[96mЕсли появляется The authenticity of host...\e[0m";
  echo -e "\n\e[96mПодключитесь локально один раз к серверу по SSH, чтобы добавить хост в файл .ssh/known_hosts\e[0m";
fi

# Если ключ получил через env SSH_KEY
if [ ! "$SSH_KEY" = '' ]; then
  eval $(ssh-agent -s)

  echo "$SSH_KEY" | tr -d '\r' | ssh-add - > /dev/null
  echo -e "\n\e[92mSSH ключ добавлен\e[0m";

  ## Create the SSH directory and give it the right permissions
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh

  ## Use ssh-keyscan to scan the keys of your private server and add it to known_host file
  ssh-keyscan $HOST_SITE_NAME_OR_IP >> ~/.ssh/known_hosts
  chmod 644 ~/.ssh/known_hosts
fi