#!/bin/bash

if [ $DATABASE = "true" ]; then
  if [ -z "$DB_NAME" ]; then echo -e "\e[91mУкажите DB_NAME \e[0m"; exit 1; fi
  if [ -z "$DB_USER" ]; then echo -e "\e[91mУкажите DB_USER \e[0m"; exit 1; fi
  if [ -z "$DB_PASS" ]; then echo -e "\e[91mУкажите DB_PASS \e[0m"; exit 1; fi
  if [ -z "$DB_LOCAL_NAME" ]; then echo -e "\e[91mУкажите DB_LOCAL_NAME \e[0m"; exit 1; fi
  if [ -z "$DB_LOCAL_USER" ]; then echo -e "\e[91mУкажите DB_LOCAL_USER \e[0m"; exit 1; fi
  if [ -z "$DB_LOCAL_PASS" ]; then echo -e "\e[91mУкажите DB_LOCAL_PASS \e[0m"; exit 1; fi
  if [ -z "$DB_LOCAL_HOST" ]; then echo -e "\e[91mУкажите DB_LOCAL_HOST \e[0m"; exit 1; fi

  db_tmp_folder="database_tmp"
  db_file_name="database-deploy.sql"
  db_local_file="/$db_tmp_folder/$db_file_name"

  # Локально экспортируем базу данных
  mkdir -p /$db_tmp_folder
  mysqldump --user=$DB_LOCAL_USER --password=$DB_LOCAL_PASS --host=$DB_LOCAL_HOST $DB_LOCAL_NAME > "$db_local_file"
  echo -e "\n\e[92mБаза данных экспортирована -> $db_local_file\e[0m\n";

  # Заменяем все вхождения подстроки на новую подстроку
  if [ ! "$DB_SEARCH" = '' ]; then
    sed -i "s~$DB_SEARCH~$DB_REPLACE~g" $db_local_file
    echo -e "\e[92mЗамена подстроки '$DB_SEARCH' на '$DB_REPLACE'\e[0m\n";
  fi

  # Закачиваем базу данных во временную папку на сервере
  rsync --compress "$db_local_file" "$DESTINATION/$db_tmp_folder/"
  echo -e "\e[92mБаза данных загружена на сервер -> $DESTINATION/$db_tmp_folder/$db_file_name\e[0m\n";

  # На сервере импортируем базу данных, после удаляем временную папку
  ssh $HOST "cd $DEPLOYMENT_SERVER_PATH && mysql -u $DB_USER --password=$DB_PASS $DB_NAME < $db_tmp_folder/$db_file_name && rm -r $db_tmp_folder"
  echo -e "\e[92mБаза данных успешно ипортирована -> $DB_NAME\e[0m\n";

  exit 0;
fi

