# Выполняем команды на сервере
if [ ! "$AFTER_UPLOAD_SERVER_COMMANDS" = '' ] && [ "$RUN_COMMANDS" = true ]; then
  # Добавялем команду для перехода в папку с проектом
  AFTER_UPLOAD_SERVER_COMMANDS="cd $DEPLOYMENT_SERVER_PATH && $AFTER_UPLOAD_SERVER_COMMANDS";

  echo -e "\n\e[34mВыполнение команд на сервере после загрузки файлов... \e[0m";
  echo $AFTER_UPLOAD_SERVER_COMMANDS
  # Запуск команд на сервере
  ssh $HOST $AFTER_UPLOAD_SERVER_COMMANDS
fi