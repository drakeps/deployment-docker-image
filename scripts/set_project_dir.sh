#!/bin/bash

### Проверяем папку с проектом ###
# Проверяем существует ли папка $PROJECT_DIR
if [ ! -d "${PROJECT_DIR}" ]; then
  echo -e "\n\e[91mПапка с проектом \e[34m${PWD}/$PROJECT_DIR \e[91mне существует\e[0m\n";
  exit 1
fi
# Проверяем пуста ли папка $PROJECT_DIR
if [ -d "${PROJECT_DIR}" ] && [ -z "$(ls -A $PROJECT_DIR)" ]; then
  echo -e "\n\e[91mПапка с прокетом \e[34m${PWD}/$PROJECT_DIR \e[91mпуста\e[0m\n";
  exit 1
fi

# Переходим в папку с проектом
cd $PROJECT_DIR

echo -e "\n\e[34mТекущая директория - \e[93m${PWD}\e[0m\n";