#!/bin/bash

### Проверяем переменные ###
if [ -z "$HOST_USERNAME" ]; then echo -e "\e[91mУкажите HOST_USERNAME \e[0m"; exit 1; fi
if [ -z "$HOST_SITE_NAME_OR_IP" ]; then echo -e "\e[91mУкажите HOST_SITE_NAME_OR_IP \e[0m"; exit 1; fi
if [ -z "$DEPLOYMENT_SERVER_PATH" ]; then echo -e "\e[91mУкажите DEPLOYMENT_SERVER_PATH \e[0m"; exit 1; fi
if [ -z "$PROJECT_DIR" ]; then echo -e "\e[91mУкажите PROJECT_DIR \e[0m"; exit 1; fi
if [ -z "$RSYNC_MOVE" ]; then echo -e "\e[91mУкажите RSYNC_MOVE \e[0m"; exit 1; fi

### Задаем дефолтные значения ###
HOST=$HOST_USERNAME@$HOST_SITE_NAME_OR_IP
DESTINATION=$HOST:$DEPLOYMENT_SERVER_PATH

SSH_KEY=${SSH_KEY:-''}
RSYNC_EXCLUDE=${RSYNC_EXCLUDE:-''}
RUN_COMMANDS=${RUN_COMMANDS:-false}
BEFORE_UPLOAD_LOCAL_COMMANDS=${BEFORE_UPLOAD_LOCAL_COMMANDS:-''}
BEFORE_UPLOAD_SERVER_COMMANDS=${BEFORE_UPLOAD_SERVER_COMMANDS:-''}
AFTER_UPLOAD_SERVER_COMMANDS=${AFTER_UPLOAD_SERVER_COMMANDS:-''}

# Параметр - нужно ли закачивать файлы?. Его можно установить как переменную окружения или передавать аргументом
UPLOAD=${UPLOAD:-false}
# проверяем существуют ли параметры
if [ ! $# -eq 0 ]; then
  if [ $1 = "upload" ]; then
    UPLOAD=true
  fi
fi

# Параметр - нужно ли деплоить базу данных. Его можно установить как переменную окружения или передавать аргументом
DATABASE=${DATABASE:-false}
# проверяем существуют ли параметр
if [ ! $# -eq 0 ]; then
  if [ $1 = "database" ]; then
    DATABASE=true
  fi
fi