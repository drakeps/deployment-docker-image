# Выполняем команды на локалке
if [ ! "$BEFORE_UPLOAD_LOCAL_COMMANDS" = '' ] && [ "$RUN_COMMANDS" = true ]; then
  echo -e "\n\e[34mВыполнение команд до загрузки на локалке.. \e[0m";
  bash -cx "$BEFORE_UPLOAD_LOCAL_COMMANDS"
fi