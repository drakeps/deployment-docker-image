#!/bin/bash

# Конвертируем строку RSYNC_MOVE (в которой указаны папки/файлы через запятую, которые будем передавать на сервер) в массив
source_items=(`echo $RSYNC_MOVE | tr "," "\n"`)
# Конвертируем строку RSYNC_EXCLUDE (в которой указаны папки/файлы через запятую, которые не будут переданы на сервер) в массив
exclude_items=(`echo $RSYNC_EXCLUDE | tr "," "\n"`)

rsync_params=()

### Собираем исключенные файлы/папки и записываем в rsync_params ###
echo -e "\n\e[34mИсключенные папки/файлы: \e[0m";
for exclude in ${exclude_items[*]} 
do
  echo -e "\e[92m- $exclude \e[0m";
  rsync_params+=("--exclude=$exclude")
done

# Файлы на сервер не закачиваются, а выводится список файлов которые могут быть закачены или удалены на сервере
if [ $UPLOAD = "false" ]; then
  rsync_params+=("--dry-run")
fi

# Меняем разрешение файлов и папок на 644 и 775 соответственно
rsync_params+=("--chmod=D755,F644")

### Загрузка файлов на сервер ###
echo -e "\n\e[34mНачало загрузки файлов на сервер - \e[93m$DESTINATION\e[0m";
for source in ${source_items[*]} 
do
  echo -e "\n\e[34mЗагрузка \e[93m$source \e[0m\n";

  # Если не сущеcтвует папки/файла $source, возварщемся в начало цикла
  if [ ! -d "${source}" ] && [ ! -f "${source}" ]; then
    echo -e "\e[91mПапка/файл $source не существует\e[0m\n";
    continue
  fi

  # Если это папка и она пуста, возварщемся в начало цикла
  if [ -d "${source}" ] && [ -z "$(ls -A $source)" ]; then
    echo -e "\e[91mПапка $source пуста\e[0m\n";
    continue
  fi

  # копируем файлы на сервер
  rsync --checksum --compress --verbose --recursive --relative --human-readable --links --delete --delete-after "${rsync_params[@]}" "$source" "$DESTINATION"
done

if [ $UPLOAD = "false" ]; then
  echo -e "\n\e[34m!! Файлы на сервер не закачиваются. Чтобы закачать добавьте параметр \e[93mupload\e[34m, либо установите переменную \e[93mUPLOAD:true \e[0m\n";
else
  echo -e "\n\e[92mФайлы успешно загружены на сервер - \e[93m$DESTINATION\e[0m\n";
fi