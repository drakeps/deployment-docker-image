# Выполняем команды на сервере
if [ ! "$BEFORE_UPLOAD_SERVER_COMMANDS" = '' ] && [ "$RUN_COMMANDS" = true ]; then
  # Добавялем команду для перехода в папку с проектом
  BEFORE_UPLOAD_SERVER_COMMANDS="cd $DEPLOYMENT_SERVER_PATH && $BEFORE_UPLOAD_SERVER_COMMANDS";

  echo -e "\n\e[34mВыполнение команд на сервере до загрузки файлов... \e[0m";
  echo $BEFORE_UPLOAD_SERVER_COMMANDS
  # Запуск команд на сервере
  ssh $HOST $BEFORE_UPLOAD_SERVER_COMMANDS
fi