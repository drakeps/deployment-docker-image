FROM composer

# install packages
RUN apk --update add nodejs npm
RUN apk add openssh-client
RUN apk add rsync
RUN apk add mysql-client

COPY scripts /scripts
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]