#!/bin/bash
set -eu

source /scripts/set_variable.sh
source /scripts/set_project_dir.sh
source /scripts/set_ssh.sh
source /scripts/database.sh
source /scripts/run_local_commands.sh
source /scripts/run_remote_commands_before_upload.sh
source /scripts/rsync.sh
source /scripts/run_remote_commands_after_upload.sh

exit 0;