#!/bin/bash

PROCESS_NAME_PRODUCTION='sitename'
PROCESS_NAME_RESERVE='sitename-reserve'
PRODUCTION_SERVER_PORT='1105'
RESERVE_SERVER_PORT='2205'

check_server_staus()
{
  i=1
  while [ $i -le 10 ]
  do
    STATUS=$(curl -s -o /dev/null -w '%{http_code}' $1)
    if [ $STATUS -eq 200 ]; then
      echo "\e[92m$i. Сервер запущен $1. Статус: $STATUS \e[0m";  
      return
    else
      echo "\e[34m$i. Проверка сервера $1. Статус: $STATUS \e[0m";
    fi
    i=$(($i+1))
    sleep 2
  done
  
  echo "\n\e[91mОшибка при запуске сервера: $1 \e[0m"
  exit 1
}
  
# проверяем существуют ли параметры
if [ ! $# -eq 0 ]; then

  if [ $1 = "reserve-server" ]; then
    echo "\n\e[34mКопирование и запуск резервного сервера... \e[0m"
    rsync -az --delete --exclude='reserve_backup' ./ ./reserve_backup
    cd reserve_backup
    pm2 delete $PROCESS_NAME_RESERVE &>/dev/null
    sleep 2
    pm2 start npm --name $PROCESS_NAME_RESERVE -- run start -- --port $RESERVE_SERVER_PORT
    check_server_staus "http://localhost:$RESERVE_SERVER_PORT"
    sleep 5
  fi

  if [ $1 = "production-server" ]; then
    echo "\n\e[34mЗапуск основного сервера... \e[0m";
    pm2 delete $PROCESS_NAME_PRODUCTION &>/dev/null
    npm install
    npm run build
    pm2 start npm --name $PROCESS_NAME_PRODUCTION -- run start
    check_server_staus "http://localhost:$PRODUCTION_SERVER_PORT"
    sleep 5

    echo "\n\e[34mОтключение резервного сервера... \e[0m";
    pm2 delete $PROCESS_NAME_RESERVE &>/dev/null

    echo "\n\e[92mDeploy завершен! \e[0m\n";
  fi
fi